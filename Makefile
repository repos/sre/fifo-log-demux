CMDDIR = ./cmd
OUTDIR = ./bin
GOARCH ?= amd64
PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
GOOS ?= linux
CGO_ENABLED ?= 0
GOFLAGS ?= CGO_ENABLED=$(CGO_ENABLED) GOARCH=$(GOARCH) GOOS=$(GOOS)

all: $(OUTDIR)/fifo-log-demux $(OUTDIR)/fifo-log-tailer

$(OUTDIR)/%:
	install -dm755 $(OUTDIR)
	$(GOFLAGS) go build -o $@ $(CMDDIR)/$*/$*.go

clean:
	go clean
	rm -rf $(OUTDIR)

dep:
	go mod download

install: all
	install -dm755 $(DESTDIR)/$(BINDIR)
	install -Dm755 $(OUTDIR)/fifo-log-demux  -t $(DESTDIR)/$(BINDIR)
	install -Dm755 $(OUTDIR)/fifo-log-tailer -t $(DESTDIR)/$(BINDIR)


.PHONY: all clean dep install
