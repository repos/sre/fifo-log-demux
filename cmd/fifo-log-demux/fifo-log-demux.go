package main

import (
	"bufio"
	"context"
	"flag"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	logFifoPath       = flag.String("log-fifo", "/var/run/fifo.pipe", "named pipe to read from")
	socketPath        = flag.String("socket", "/var/run/log.socket", "socket for local connections")
	prometheusAddress = flag.String("prometheus-address", ":8888", "address used for prometheus http server")
	inputBufferSize   = flag.Int("input-buffer-size", 1_000_000, "input buffer size (in number of messages)")
	outputBufferSize  = flag.Int("output-buffer-size", 1_000_000, "output buffer size (per client, in number of messages)")
	inputMessages     = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "fifo_log_demux_input_messages_total",
		Help: "Total messages received by fifo_log_demux",
	}, []string{
		"status",
	})
	outputMessages = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "fifo_log_demux_output_messages_total",
		Help: "Total messages sent by fifo_log_demux",
	}, []string{
		"status",
	})
)

// Server listens on a UNIX socket and multiplexes data read from a pipe Reader
// onto eventual connections. Each connection has an associated optional
// regexp. If a client specifies a regular expression, only matching log lines
// will be sent to the client. Data is consumed from the pipe Reader even if
// there are no connections. It is assumed that clients can consume input as
// quickly as it is written, otherwise the whole Server will block waiting for
// them (which isn't a particularly clever design, but it works for the
// intended use case).
type Server struct {
	logFifoPath string
	broker      *Broker
	listener    *net.UnixListener
}

func NewServer(logFifoPath, socketPath string, broker *Broker) (*Server, error) {
	os.Remove(socketPath)

	l, err := net.ListenUnix("unix", &net.UnixAddr{socketPath, "unix"})
	if err != nil {
		return nil, err
	}
	return &Server{
		logFifoPath: logFifoPath,
		listener:    l,
		broker:      broker,
	}, nil
}

func (s *Server) readLogs(ctx context.Context) {
	processed := prometheus.Labels{"status": "processed"}
	discarded := prometheus.Labels{"status": "discarded"}
	for {
		// By keeping this in an infinite loop fifo-log-demux is able to re-open
		// the logFifoPath after an EOF that's usually received cause the other side of
		// the pipe has been closed
		f, err := os.Open(s.logFifoPath)
		if err != nil {
			log.Println(err)
			return
		}
		defer f.Close()

		scanner := bufio.NewScanner(f)

		for scanner.Scan() {
			// Read data as fast as possible, discarding messages if needed
			data := scanner.Bytes()
			// We need a clone of the slice to be injected on the publish channel
			// or we would have concurrency issues between the goroutines sending
			// data to the clients and this one
			// slices is only available on go >=1.21
			//publishData := slices.Clone(data)
			publishData := append(data[:0:0], data...)
			select {
			case <-ctx.Done():
				return
			default:
			}

			if ok := s.broker.Publish(publishData); ok {
				inputMessages.With(processed).Inc()
			} else {
				inputMessages.With(discarded).Inc()
			}
		}
	}
}

func (s *Server) Run(ctx context.Context) {
	buf := make([]byte, 65536)

	go func(ctx context.Context) {
		select {
		case <-ctx.Done():
			s.listener.Close()
			s.broker.Stop()
		}
	}(ctx)

	go s.broker.Start()
	go s.readLogs(ctx)

	for {
		conn, err := s.listener.AcceptUnix()
		if err != nil {
			return
		}

		// Read optional regex specified by the client program
		n, err := conn.Read(buf[:])
		if err != nil {
			returnError(err, conn)
			continue
		}

		regex, err := regexp.Compile(strings.TrimSuffix(string(buf[:n]), "\n"))
		if err != nil {
			// The client-supplied regular expression cannot be parsed. Return
			// an error and close the connection.
			returnError(err, conn)
			continue
		}

		// one regex per connection
		c := NewClient(conn, regex, s.broker)
		go c.sendLogs(ctx)
	}
}

type Client struct {
	conn   net.Conn
	r      *regexp.Regexp
	broker *Broker
	msgCh  chan []byte
}

func NewClient(conn net.Conn, regex *regexp.Regexp, broker *Broker) *Client {
	return &Client{
		conn:   conn,
		r:      regex,
		broker: broker,
		msgCh:  broker.Subscribe(),
	}
}

func (c *Client) sendLogs(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			c.Close()
			return
		case data := <-c.msgCh:
			if c.r.Match(data) {
				if _, err := c.conn.Write(append(data, byte('\n'))); err != nil {
					if opErr, ok := err.(*net.OpError); ok {
						if syscallErr, ok := opErr.Err.(*os.SyscallError); ok {
							if errno, ok := syscallErr.Err.(syscall.Errno); ok && errno != syscall.EPIPE {
								log.Println("Error writing to client connection:", err)
							}
						}
					}
					c.Close()
					return
				}
			}
		}
	}
}

func (c *Client) Close() {
	c.conn.Close()
	c.broker.Unsubscribe(c.msgCh)
}

// Broker handles the fan out from the Server reading the FIFO to all the
// Subscribers (Clients) connected to the UNIX socket.
type Broker struct {
	stopCh    chan struct{}
	publishCh chan []byte
	subCh     chan chan []byte
	unsubCh   chan chan []byte
}

func NewBroker() *Broker {
	return &Broker{
		stopCh:    make(chan struct{}),
		publishCh: make(chan []byte, *inputBufferSize),
		subCh:     make(chan chan []byte, 1),
		unsubCh:   make(chan chan []byte, 1),
	}
}

func (b *Broker) Start() {
	discarded := prometheus.Labels{"status": "discarded"}
	demux := prometheus.Labels{"status": "demux"}
	clients := map[chan []byte]struct{}{}
	for {
		select {
		case <-b.stopCh:
			return
		case msgCh := <-b.subCh:
			clients[msgCh] = struct{}{}
		case msgCh := <-b.unsubCh:
			delete(clients, msgCh)
		case msg := <-b.publishCh:
			for msgCh := range clients {
				select {
				case msgCh <- msg:
					outputMessages.With(demux).Inc()
				default:
					outputMessages.With(discarded).Inc()
				}
			}
		}
	}
}

func (b *Broker) Stop() {
	close(b.stopCh)
}

func (b *Broker) Subscribe() chan []byte {
	msgCh := make(chan []byte, *outputBufferSize)
	b.subCh <- msgCh
	return msgCh
}

func (b *Broker) Unsubscribe(msgCh chan []byte) {
	b.unsubCh <- msgCh
}

func (b *Broker) Publish(msg []byte) bool {
	select {
	case b.publishCh <- msg:
		return true
	default:
		return false
	}
}

func returnError(err error, conn net.Conn) {
	conn.Write([]byte(err.Error()))
	conn.Close()
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	// Special exit code here is to exempt systemd service from continually
	// trying to restart the service; Something is fundamentally broken and
	// attempts at restarts are futile. Trapping SIGUSR2 will provide easy
	// testing to make sure the service does not continually restart.
	//
	// Keep the exit code in sync with the systemd service unit
	// RestartPreventExitStatus= definition.
	ctx, _ := signal.NotifyContext(context.Background(), syscall.SIGUSR2)

	http.Handle("/metrics", promhttp.Handler())
	promServer := http.Server{Addr: *prometheusAddress}
	go func() {
		promServer.ListenAndServe()
	}()

	broker := NewBroker()
	srv, err := NewServer(*logFifoPath, *socketPath, broker)
	if err != nil {
		log.Print(err)
		os.Exit(70)
	}
	log.Println("Waiting for connections on", *socketPath)
	srv.Run(ctx)
	promServer.Shutdown(ctx)
	select {
	case <-ctx.Done():
		os.Exit(70)
	default:
		os.Exit(0)
	}
}
